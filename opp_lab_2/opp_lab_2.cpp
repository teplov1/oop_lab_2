﻿#include <iostream>
#include <vector>
#include <string>
#include <complex>
#include "LongNum.h"
#include "methods.h"
Multip* LongNum::meth;
Revers* LongNum::rev_meth;
int base1 = 10;
class NUMBER_IS_ZERO{};
class NUMBER_IS_EMPTY{};
class NUMBER_IS_NEGATIVE{};
class NOT_NUMBER{};
void error_check(std::string n1, std::string n2)
{
    std::size_t found;
    if (n1 == "" || n2 == "")
        throw new NUMBER_IS_EMPTY;
    if (n1 == "0" || n2 == "0")
        throw new NUMBER_IS_ZERO;
    if (n1[0] == '-' || n2[0] == '-')
        throw new NUMBER_IS_NEGATIVE;
    for(int i=0;i<n1.size();i++)
        if (!std::isdigit(n1[i]))
        {
            throw new NOT_NUMBER;;
        }
    for(int i=0;i<n2.size();i++)
        if (!std::isdigit(n2[i]))
        {
            throw new NOT_NUMBER;;
        }
}
void output(std::vector<int> vec)
{
    for (int i = vec.size()-1; i >=0 ; i--)
    {
        std::cout << vec[i];
    }
}
int main()
{
    try {
        std::cout << "Hi! I'm skynet 1.0, an advanced calculator (as for now...) .\n";
        std::cout << "I can calculate various operations between relatively big numbers using different methods.\n";
        LongNum n1, n2, n3;
        std::string str1 = "5706", str2 = "104857610489871048987104898710489871048987";
        error_check(str1, str2);
        std::cout << "Numbes currently in use: \n"<<"LongNum1: "<<str1<<"\n";
        std::cout << "LongNum2: "<<str2<<"\n\n";
        n1.set_val(str1);
        n2.set_val(str2);
        LongNum::Method(new karatsuba);
        n1.precisions = 10;
        n3 = n1 * n2;
        std::cout << n3.get_val() << "\n";
        LongNum::Method(new toom_3);
        n3 = n1 * n2;
        std::cout << n3.get_val() << "\n";
        LongNum::Method(new Strassen);
        n3 = n1 * n2;
        std::cout << n3.get_val() << "\n";
        LongNum::Reverse(new Cook_reversed);
        n3 = n1 / 1;
        std::cout << n3.get_val() << "\t";
        /*for (long long i = pow(2,20); i < pow(2, 30); i++)
        {
            for (long long j = pow(2, 20); j < pow(2, 30); j++)
            {
                n1.set_val(std::to_string(i));
                n2.set_val(std::to_string(j));
                n3 = n1 * n2;
                if (n3.get_val() != std::to_string(i * j))
                {
                    std::cout << "trouble "<<i<<" "<<j<<"\n";
                    std::cout <<"n3 " <<n3.get_val()<<"\t";
                    std::cout << "i*j " << std::to_string(i * j) << "\n";
                    break;
                }
                else
                {
                    std::cout << "DONE" << "\n";
                }
            }
        }*/
    }
    catch (NUMBER_IS_EMPTY*)
    {
        std::cout << "first and/or second number is not inputed. \n>>>>>>>>>> PLEASE RESTART PROGRAM <<<<<<<<<<";
    }
    catch (NUMBER_IS_ZERO*)
    {
        std::cout << "first and/or second number is zero. \n>>>>>>>>>> PLEASE RESTART PROGRAM <<<<<<<<<<";
    }
    catch (NUMBER_IS_NEGATIVE*)
    {
        std::cout << "first and/or second number is negative. \n>>>>>>>>>> PLEASE RESTART PROGRAM <<<<<<<<<<\n";
    }
    catch (NOT_NUMBER*)
    {
        std::cout << "first and/or second word is not number. \n>>>>>>>>>> PLEASE RESTART PROGRAM <<<<<<<<<<\n";
    }
    
}

