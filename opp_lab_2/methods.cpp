#include "methods.h"
#include <iostream>
#include <vector>
#include <string>
#include <complex>

const double pi = std::acos(-1);
const std::complex<double> i(0, 1);
int power_of_10_m5 = 0;
int base2 = 1;


std::vector<int> karatsuba::multiply(std::vector<int> min, std::vector<int> max)
{
	if (min.size() > max.size())
		return this->multiply(max, min);
	std::vector<int> x1, x0;
	std::vector<int> y1, y0;
	std::vector<int> y1byx1, y0byx0, x1_x0byy1_y0;
	std::vector<int> y1_y0, x1_x0;
	std::vector<int> res_pow_1, res_pow_2, res_pow_0;
	std::vector<int> res;
	long size = min.size();
	long actual_size = size;
	if (size < 3)
	{
		res = LongNum::multipl(min, max);
		return res;
	}
	size = size - size % 2;
	for (long i = 0; i < size / 2; i++)
	{
		x0.push_back(min[i]);
		y0.push_back(max[i]);
	}
	for (int i = size / 2; i < actual_size; i++)
	{
		x1.push_back(min[i]);
		y1.push_back(max[i]);
	}
	for (int i = actual_size; i < max.size(); i++)
	{
		y1.push_back(max[i]);
	}
	y1byx1 = this->multiply(x1, y1);
	y0byx0 = this->multiply(x0, y0);

	y1_y0 = LongNum::add(y1, y0);
	x1_x0 = LongNum::add(x1, x0);

	x1_x0byy1_y0 = this->multiply(y1_y0, x1_x0);
	x1_x0byy1_y0 = LongNum::subs(x1_x0byy1_y0, y1byx1);
	x1_x0byy1_y0 = LongNum::subs(x1_x0byy1_y0, y0byx0);
	for (int i = 0; i < size; i++)
	{
		res_pow_2.push_back(0);
	}
	for (int i = 0; i < size / 2; i++)
	{
		res_pow_1.push_back(0);
	}
	res_pow_2.insert(std::end(res_pow_2), std::begin(y1byx1), std::end(y1byx1));
	res_pow_1.insert(std::end(res_pow_1), std::begin(x1_x0byy1_y0), std::end(x1_x0byy1_y0));
	res_pow_0 = y0byx0;

	res = LongNum::add(res_pow_2, res_pow_1);
	res = LongNum::add(res, res_pow_0);
	return res;
}
std::vector<int> toom_3::multiply(std::vector<int> min, std::vector<int> max)
{
	if (min.size() > max.size())
		return this->multiply(max, min);
	std::vector<int> x2, x1, x0;
	std::vector<int> y2, y1, y0;
	std::vector<int> y2byx2, y1byx1, y0byx0, z10, z20, z21;
	std::vector<int> y2_y1, y2_y0, y1_y0, x1_x0, x2_x0, x2_x1;
	std::vector<int> res_pow_m1, res_pow_m2, res_pow_0, res_pow_2m1, res_pow_2m2, res_pow_m2m1;
	std::vector<int> res;

	int size = std::min(min.size(), max.size());
	int actual_size = size;

	if (size < 4)
	{
		return LongNum::multipl(min, max);
	}
	size = size - size % 3;
	for (int i = 0; i < size / 3; i++)
	{
		x0.push_back(min[i]);
		y0.push_back(max[i]);
	}
	for (int i = size / 3; i < 2 * size / 3; i++)
	{
		x1.push_back(min[i]);
		y1.push_back(max[i]);
	}
	for (int i = 2 * size / 3; i < actual_size; i++)
	{
		x2.push_back(min[i]);
		y2.push_back(max[i]);
	}
	for (int i = actual_size; i < max.size(); i++)
	{
		y2.push_back(max[i]);
	}
	y2byx2 = this->multiply(x2, y2);
	y1byx1 = this->multiply(x1, y1);
	y0byx0 = this->multiply(x0, y0);

	x2_x1 = LongNum::add(x2, x1);
	x2_x0 = LongNum::add(x2, x0);
	x1_x0 = LongNum::add(x1, x0);
	y2_y1 = LongNum::add(y2, y1);
	y2_y0 = LongNum::add(y2, y0);
	y1_y0 = LongNum::add(y1, y0);

	z10 = this->multiply(y1_y0, x1_x0);
	z10 = LongNum::subs(z10, y1byx1);
	z10 = LongNum::subs(z10, y0byx0);

	z20 = this->multiply(y2_y0, x2_x0);
	z20 = LongNum::subs(z20, y2byx2);
	z20 = LongNum::subs(z20, y0byx0);

	z21 = this->multiply(y2_y1, x2_x1);
	z21 = LongNum::subs(z21, y2byx2);
	z21 = LongNum::subs(z21, y1byx1);

	for (int i = 0; i < size / 3; i++)
	{
		res_pow_m1.push_back(0);
	}
	for (int i = 0; i < 2 * size / 3; i++)
	{
		res_pow_2m1.push_back(0);
	}
	for (int i = 0; i < 2 * size / 3; i++)
	{
		res_pow_m2.push_back(0);
	}
	for (int i = 0; i < 4 * size / 3; i++)
	{
		res_pow_2m2.push_back(0);
	}
	for (int i = 0; i < 3 * size / 3; i++)
	{
		res_pow_m2m1.push_back(0);
	}
	res_pow_m1.insert(std::end(res_pow_m1), std::begin(z10), std::end(z10));
	res_pow_2m1.insert(std::end(res_pow_2m1), std::begin(y1byx1), std::end(y1byx1));
	res_pow_m2.insert(std::end(res_pow_m2), std::begin(z20), std::end(z20));
	res_pow_2m2.insert(std::end(res_pow_2m2), std::begin(y2byx2), std::end(y2byx2));
	res_pow_m2m1.insert(std::end(res_pow_m2m1), std::begin(z21), std::end(z21));
	res_pow_0 = y0byx0;
	res = LongNum::add(res_pow_2m2, res_pow_m2);
	res = LongNum::add(res, res_pow_2m1);
	res = LongNum::add(res, res_pow_m1);
	res = LongNum::add(res, res_pow_m2m1);
	res = LongNum::add(res, res_pow_0);
	return res;
}
std::vector<int> Strassen::multiply(std::vector<int> min, std::vector<int> max)
{
	if (min.size() > max.size())
		return this->multiply(max, min);

	size_t n = 1;
	while (n < std::max(max.size(), min.size()))  n <<= 1;
	n <<= 1;
	min.resize(n);
	max.resize(n);
	std::vector<std::complex<double>> min1;
	std::vector<std::complex<double>> max1;
	std::vector<std::complex<double>> half_res;
	std::vector<int> res;
	for (int i = 0; i < n; i++)
	{
		std::complex<double> m(min[i], 0);
		min1.push_back(m);
	}
	for (int i = 0; i < n; i++)
	{
		std::complex<double> m(max[i], 0);
		max1.push_back(m);
	}
	min1 = FFT(min1, false);
	max1 = FFT(max1, false);
	half_res = complex_mult(min1, max1);
	half_res = FFT(half_res, true);
	return format_complex(half_res);
}
std::vector<int> Cook_reversed::reverse(std::vector<int> num,int precision,int& power_of_10_m5)
{
	std::vector<int> xn = {1};
	std::vector<int> new_xn ;
	std::vector<int> xn1 = {1};
	std::vector<int> xn_1;
	std::vector<int> two = { 2 };
	std::vector<int> twobyxn;
	std::vector<int> xnbynum;
	std::vector<int> xnbyxn;
	int k= num.size()-2;
	power_of_10_m5 = num.size() - 1;
	while (xn.size() < precision)
	{
		for (int i = 0; i < pow(2, k); i++)
			new_xn.push_back(0);
		new_xn.push_back(1);
		k++;
		xn1 = LongNum::multipl(xn, new_xn);
		new_xn.clear();
		twobyxn = LongNum::multipl(two, xn1);
		xnbyxn = LongNum::multipl(xn, xn);
		xnbynum = LongNum::multipl(num, xnbyxn);
		xn = LongNum::subs(twobyxn, xnbynum);
	}
	new_xn.insert(std::end(new_xn), std::end(xn)-precision, std::end(xn));
	xn = new_xn;
	new_xn.clear();
	k = num.size() +precision- 1;
	for (int i = 0; i < k; i++)
		new_xn.push_back(0);
	new_xn.push_back(1);
	xn1 = LongNum::multipl(xn, new_xn);
	new_xn.clear();
	do
	{
		xn_1 = xn;
		twobyxn = LongNum::multipl(two, xn1);
		xnbyxn = LongNum::multipl(xn, xn);
		xnbynum = LongNum::multipl(num, xnbyxn);
		xn = LongNum::subs(twobyxn, xnbynum);
		new_xn.insert(std::end(new_xn), std::end(xn) - precision, std::end(xn));
		xn = new_xn;
		new_xn.clear();
		for (int i = 0; i < k; i++)
			new_xn.push_back(0);
		new_xn.push_back(1);
		xn1 = LongNum::multipl(xn, new_xn);
		new_xn.clear();
	} while (xn != xn_1);
	return xn;
}

std::vector<int> Strassen::format_complex(std::vector<std::complex<double>> com)
{
	std::vector <int> res;
	for (int i = 0; i < com.size(); i++)
	{
		res.push_back(int(com[i].real() / com.size() + 0.5));
	}
	res.push_back(0);
	for (int j = 0; j < res.size() - 1; j++)
	{
		res[j + 1] += res[j] / (int)pow(10, base2);
		res[j] = res[j] % (int)pow(10, base2);
	}
	while (res.size() > 1 && res.back() == 0)
		res.pop_back();
	return res;
}

std::vector<std::complex<double>> Strassen::FFT(const std::vector<std::complex<double>> a, bool inver)
{
	int s = (int)a.size();
	std::vector<std::complex<double>> a0 = {}, a1 = {};
	std::vector<std::complex<double>> y0, y1, y;
	for (int i = 0; i < s; i++)
		y.push_back(0);
	if (s < 2)
	{
		return a;
	}
	std::complex<double> wn = std::exp((2 * pi / s) * i);
	if (inver)
		wn = conj(wn);
	std::complex<double> w(1, 0);
	for (int j = 0; j < s; j += 2)
	{
		a0.push_back(a[j]);
		a1.push_back(a[j + 1]);
	}
	y0 = FFT(a0, inver);
	y1 = FFT(a1, inver);
	for (int j = 0; j < (s / 2); j++)
	{
		y[j] = y0[j] + w * y1[j];
		y[j + s / 2] = y0[j] - w * y1[j];
		w *= wn;
	}
	return y;
}
std::vector<std::complex<double>> Strassen::complex_mult(std::vector<std::complex<double>> a, std::vector<std::complex<double>> b)
{
	std::vector<std::complex<double>> sum;
	for (int i = 0; i < a.size(); i++)
	{
		std::complex<double> c1(a[i].real(), a[i].imag());
		std::complex<double> c2(b[i].real(), b[i].imag());
		sum.push_back(0);
		sum[i] = c1 * c2;
	}
	return sum;
}