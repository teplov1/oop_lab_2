#pragma once
#include "LongNum.h"
#include <vector>
#include <string>
#include <complex>
class karatsuba: public Multip {
public:
	std::vector<int> multiply(std::vector<int> min, std::vector<int> max);
};
class toom_3 : public Multip {
public:
	std::vector<int> multiply(std::vector<int> min, std::vector<int> max);
};
class Strassen : public Multip {
private:
	std::vector<int> format_complex(std::vector<std::complex<double>> com);
	std::vector<std::complex<double>> FFT(std::vector<std::complex<double>> a, bool inver);
	std::vector<std::complex<double>> complex_mult(std::vector<std::complex<double>> a, std::vector<std::complex<double>> b);
public:
	std::vector<int> multiply(std::vector<int> min, std::vector<int> max);
};
class Cook_reversed : public Revers {
public:
	std::vector<int> reverse(std::vector<int> min, int precision, int& power_of_10_m5);
};
