#pragma once
#include <vector>
#include <string>
#include <complex>
class LongNum;
class Multip {
public:
	virtual std::vector<int> multiply(std::vector<int> min, std::vector<int> max) = 0;
}; 
class Revers {
public:
	virtual std::vector<int> reverse(std::vector<int> num, int precision, int& tp10) = 0;
};
class LongNum
{
public:
	//int methods;
	int precisions = 3;
	std::string get_val();
	std::vector<int> get_vecval();
	void set_val(std::string val);
	void set_vecval(std::vector<int> val);
	void to_str_reversed(int tp10);
	LongNum operator*(LongNum secnum);
	LongNum operator/(int secnum);
	static std::vector<int> multipl(std::vector<int> num1, std::vector<int> num2);
	static std::vector<int> add(std::vector<int> num1, std::vector<int> num2);
	static std::vector<int> subs(std::vector<int> num1, std::vector<int> num2);
	static void Method(Multip* mult);
	static void Reverse(Revers* mult);
protected:
	static Multip* meth;
	static Revers* rev_meth;
	std::string value;
	std::vector<int> vecvalue;
	void to_vec();
	void to_str();
};
//Multip* LongNum::meth;