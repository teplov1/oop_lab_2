#include "LongNum.h"
#include <iostream>
#include <vector>
#include <string>
#include <complex>
int base=1;
std::string value="";
std::vector<int> vecvalue = {};
int counter = 0;
//int methods = 0;


std::vector<int> LongNum::multipl(std::vector<int> min, std::vector<int> max)
{
	std::vector<int> num_super;
	for (int i = 0; i < max.size()+ min.size()+1; i++)
		num_super.push_back(0);
	for (int i = 0; i < min.size(); i++)
	{
		for (int j = 0; j < max.size(); j++)
		{
			num_super[i + j] += min[i] * max[j];
			num_super[i + j + 1] += num_super[i + j] / (int)pow(10,base);
			num_super[i + j] = num_super[i + j] %(int)pow(10, base);
		}
	}
	while (num_super.size() > 1 && num_super.back() == 0)
		num_super.pop_back();
	return num_super;
}
std::vector<int> LongNum::add(std::vector<int> min, std::vector<int> max)
{
	
	if (min.size() > max.size())
		return add(max, min);
	max.push_back(0);
	for (int i = 0; i < min.size(); i++)
	{
		max[i] += min[i];
		if (max[i] >= (int)pow(10, base))
		{
			max[i + 1]++;
			max[i] = max[i] % (int)pow(10, base);
		}
	}
	int i = min.size();
	while (max[i] >= (int)pow(10, base))
	{
		max[i + 1]++;
		max[i] = max[i] % (int)pow(10, base);
		i++;
	}
	while (max.size() > 1 && max.back() == 0)
		max.pop_back();
	return max;
}
std::vector<int> LongNum::subs(std::vector<int> min, std::vector<int> max)
{
	if (min.size() > max.size())
		return subs(max, min);
	std::vector<int> res = max;
	for (int i = 0; i < min.size(); i++)
	{
		res[i] -= min[i];
		if (res[i] < 0)
		{
			if (i==res.size()-1)
				return subs(max, min);
			res[i + 1]--;
			res[i] += (int)pow(10, base);
		}
	}
	while (res.size() > 1 && res.back() == 0)
		res.pop_back();
	return res;
}
std::string LongNum::get_val()
{
	if(this->value=="")
		this->to_str();
	return this->value;
}
std::vector<int> LongNum::get_vecval()
{
	return this->vecvalue;
}
void LongNum::set_vecval(std::vector<int> val)
{
	this->vecvalue = val;
}
void LongNum::set_val(std::string val)
{
	this->value = val;
}
void LongNum::to_vec()
{
	std::vector<int> number;
	for (int i = this->value.size(); i > 0; i-=base)
	{
		if (i < base) {
			number.push_back(atoi(this->value.substr(0, i).c_str()));
			i = -1;
		}
		else {
			number.push_back(atoi(this->value.substr(i-base, base).c_str()));
		}
	}
	this->vecvalue.insert(std::end(this->vecvalue), std::begin(number), std::end(number));
}
void LongNum::to_str()
{
	std::string val="";
	for (int i = this->vecvalue.size() - 1; i >= 0; i--)
	{
		if (std::to_string(this->vecvalue[i]).size() < base&&i!= this->vecvalue.size() - 1)
		{
			for (int j = 0; j < base - std::to_string(this->vecvalue[i]).size(); j++)
				val += "0";
		}
		val += std::to_string(this->vecvalue[i]);
	}
	this->set_val(val);
}
void LongNum::to_str_reversed(int power_of_10_m5)
{
	std::string val = "0.";
	for (int i = 0; i < power_of_10_m5; i++)
		val += "0";
	this->to_str();
	val += this->value;
	this->set_val(val);
}
void LongNum::Method(Multip* mult)
{
	meth = mult;
}
void LongNum::Reverse(Revers* re)
{
	rev_meth = re;
}
LongNum LongNum::operator/(int secnum) {
	std::vector<int> newvecnum;
	LongNum newnum;
	this->vecvalue = {};
	this->to_vec();
	newvecnum=rev_meth->reverse(this->vecvalue, this->precisions, counter);
	newnum.set_vecval(newvecnum);
	newnum.to_str_reversed(counter);
	return newnum;
}
LongNum LongNum::operator*(LongNum secnum) {
	std::vector<int> newvecnum;
	LongNum newnum;
	this->vecvalue = {};
	this->to_vec();
	secnum.to_vec();
	newvecnum = meth->multiply(this->vecvalue, secnum.vecvalue);
	newnum.set_vecval(newvecnum);
	return newnum;
}
